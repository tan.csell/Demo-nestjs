import { environment } from '@app/shared/environments';
import { DynamicModule, Logger, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({})
export class PostgresqlProvider {
    static register(): DynamicModule {
        Logger.log('[PostgresqlConfig] Postgresql connection established!');
        return {
            module: PostgresqlProvider,
            exports: [PostgresqlProvider],
            imports: [
                SequelizeModule.forRoot({
                    uri: environment.postgresUri,
                    autoLoadModels: true,
                    synchronize: true,
                    benchmark: true,
                    logging: true,
                    // sync: { alter: true }
                }),
            ],
        };
    }
}
