import { DynamicModule, Module } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';

@Module({})
export class EventEmitterProvider {
    static register(): DynamicModule {
        return {
            module: EventEmitterProvider,
            exports: [EventEmitterProvider],
            imports: [
                EventEmitterModule.forRoot({
                    maxListeners: 20,
                }),
            ],
        };
    }
}
