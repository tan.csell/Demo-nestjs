import { DynamicModule, Logger, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

@Module({})
export class ScheduleProvider {
    static register(): DynamicModule {
        Logger.log('[ScheduleConfig] Schedule module established!');
        return {
            module: ScheduleProvider,
            exports: [ScheduleProvider],
            imports: [ScheduleModule.forRoot()],
        };
    }
}
