import { Module } from '@nestjs/common';
import { RedisProvider } from './redis.config';
import { ErrorHandlerProvider } from './error-handler.config';
import { PostgresqlProvider } from './postgresql.config';
import { EventEmitterProvider } from './event-emitter.config';
import { ScheduleProvider } from './schedule.config';

@Module({
    imports: [
        RedisProvider.register(),
        ScheduleProvider.register(),
        PostgresqlProvider.register(),
        EventEmitterProvider.register(),
        ErrorHandlerProvider.register(),
    ],
    providers: [],
    exports: [],
})
export class PersistentConfigModule {}
