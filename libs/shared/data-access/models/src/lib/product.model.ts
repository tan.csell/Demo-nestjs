import { ApiProperty, OmitType } from '@nestjs/swagger';

export class ProductDto {
    @ApiProperty({
        required: true,
        description: 'ID của sản phẩm',
        example: 1,
    })
    id: number;

    @ApiProperty({
        required: true,
        description: 'Mã sản phẩm',
        example: 'DV0001',
    })
    sku: string;

    @ApiProperty({
        required: true,
        description: 'Tên của sản phẩm',
        example: 'Sản phẩm 1',
    })
    name: string;

    @ApiProperty({
        required: true,
        description: 'Loại sản phẩm',
        example: 1,
    })
    type: number;

    @ApiProperty({
        required: false,
        description: 'Ảnh đại diện của sản phẩm',
        example: 'image1',
    })
    thumbnail: string;

    @ApiProperty({
        required: false,
        description: 'Danh sách ảnh của sản phẩm',
        example: ['image1', 'image2'],
    })
    images: string[];

    @ApiProperty({
        required: false,
        description: 'Trạng thái',
        example: 1,
    })
    status: number;

    @ApiProperty({
        required: true,
        description: 'Giá sản phẩm',
        example: 100000,
    })
    price: number;

    @ApiProperty({
        required: false,
        description: 'Giá gốc sản phẩm',
        example: 90000,
    })
    original_price: number;

    @ApiProperty({
        required: false,
        description: 'Giá niêm yết sản phẩm',
        example: 90000,
    })
    normal_price: number;

    @ApiProperty({
        required: true,
        description: 'Thông tin thuộc tính sản phẩm',
        example: ['1:Nhật bản', '2:Dior'],
    })
    attributes: string[]

    @ApiProperty({
        required: true,
        description: 'Danh sách danh mục',
        example: ['1:danh mục 1', '2:danh mục 2'],
    })
    categories: string[]

    @ApiProperty({
        required: true,
        description: 'Danh sách nhà cung cấp',
        example: ['1:nhà cung cấp 1', '2:nhà cung cấp 2'],
    })
    suppliers: string[]

    @ApiProperty({
        required: false,
        example: [
            {
                id: 1,
                name: "sản phẩm 1",
                sku: "SP0001", price: 10000,
                normal_price: 100000,
                master_id: 1,
                is_default: true
            }
        ],
        description: 'Thông tin phân loại',
    })
    variations: object[];

    @ApiProperty({
        required: false,
        default: true,
        description: 'Trạng thái hoạt động',
    })
    is_actived: boolean;

    @ApiProperty({ required: false, description: 'Ngày tạo' })
    created_at: Date;

    @ApiProperty({
        required: false,
        description: 'Người tạo',
        example: {
            id: 1,
            name: 'Admin',
        },
    })
    created_by: object;

    @ApiProperty({ required: false, description: 'Ngày cập nhật' })
    updated_at: Date;

    @ApiProperty({
        required: false,
        description: 'Người cập nhật',
        example: null,
    })
    updated_by: object;
}

export class ProductCreateParamsDto extends OmitType(ProductDto, [
    'id',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'is_actived',
]) { }

export class ProductUpdateParamsDto extends OmitType(ProductDto, [
    'id',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'is_actived',
]) { }
export class ProductListParamsDto {
    @ApiProperty({ default: 0, required: true })
    offset: number;

    @ApiProperty({ default: 20, required: true })
    limit: number;

    @ApiProperty({ default: 'created_at', required: false })
    sort_by?: string;

    @ApiProperty({ default: 'desc', required: false })
    order_by?: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo nhà cung cấp sản phẩm',
    })
    suppliers: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo thuộc tính sản phẩm',
    })
    attributes: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo danh mục sản phẩm',
    })
    categories: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo tên, mã sản phẩm',
    })
    product_keyword: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo trạng thái',
    })
    statuses: string;

    @ApiProperty({
        required: false,
        example: new Date(),
        description: 'Ngày bắt đầu',
    })
    min_created_at: string;

    @ApiProperty({
        required: false,
        example: new Date(),
        description: 'Ngày kết thúc',
    })
    max_created_at: string;
}
