import { ApiProperty, OmitType } from '@nestjs/swagger';

export class CategoryDto {
    @ApiProperty({
        required: true,
        description: 'ID của danh mục sản phẩm',
        example: 1,
    })
    id: number;

    @ApiProperty({
        required: true,
        description: 'Tên của danh mục sản phẩm',
        example: 'Danh mục 1',
    })
    name: string;

    @ApiProperty({
        required: false,
        description: 'Trạng thái',
        example: 1,
    })
    status: number;

    @ApiProperty({
        required: false,
        default: true,
        description: 'Trạng thái hoạt động',
    })
    is_actived: boolean;

    @ApiProperty({ required: false, description: 'Ngày tạo' })
    created_at: Date;

    @ApiProperty({
        required: false,
        description: 'Người tạo',
        example: {
            id: 1,
            name: 'Admin',
        },
    })
    created_by: object;

    @ApiProperty({ required: false, description: 'Ngày cập nhật' })
    updated_at: Date;

    @ApiProperty({
        required: false,
        description: 'Người cập nhật',
        example: null,
    })
    updated_by: object;
}

export class CategoryCreateParamsDto extends OmitType(CategoryDto, [
    'id',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'is_actived',
]) { }

export class CategoryUpdateParamsDto extends OmitType(CategoryDto, [
    'id',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'is_actived',
]) { }

export class CategoryListParamsDto {
    @ApiProperty({ default: 0, required: true })
    offset: number;

    @ApiProperty({ default: 20, required: true })
    limit: number;

    @ApiProperty({ default: 'created_at', required: false })
    sort_by?: string;

    @ApiProperty({ default: 'desc', required: false })
    order_by?: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo tên, mã danh mục sản phẩm',
    })
    category_name: string;

    @ApiProperty({
        required: false,
        description: 'Lọc theo trạng thái',
    })
    statuses: string;

    @ApiProperty({
        required: false,
        example: new Date(),
        description: 'Ngày bắt đầu',
    })
    min_created_at: string;

    @ApiProperty({
        required: false,
        example: new Date(),
        description: 'Ngày kết thúc',
    })
    max_created_at: string;
}
