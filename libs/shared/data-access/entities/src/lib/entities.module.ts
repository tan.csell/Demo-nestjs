import { Global, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
// Schemas
import { Product } from './product-schema/product.entity';
import { Category } from './product-schema/category.entity';

@Global()
@Module({
    imports: [SequelizeModule.forFeature([Product, Category])],
    exports: [SequelizeModule],
})
export class SharedEntitiesModule { }
