import * as moment from 'moment';
import { environment } from '@app/shared/environments';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Op, Sequelize } from 'sequelize';
import { isNil, omitBy, capitalize } from 'lodash';
import { HttpStatus, NotFoundException, HttpException } from '@nestjs/common';
import {
    AfterCreate,
    AfterUpdate,
    AfterDestroy,
    Column,
    CreatedAt,
    DataType,
    Model,
    Table,
    UpdatedAt,
} from 'sequelize-typescript';

@Table({ tableName: 'tbl_Categorys' })
export class Category extends Model {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id: number;

    @Column({
        type: DataType.STRING(255),
        allowNull: false,
    })
    name: string;

    @Column({
        type: DataType.STRING(255),
    })
    slug: string;

    @Column({
        type: DataType.INTEGER,
        defaultValue: 1,
    })
    status: number;

    @Column({
        type: DataType.STRING(255),
        allowNull: true,
    })
    discription: string;

    @Column({
        type: DataType.NUMBER,
        defaultValue: null,
    })
    parent_id: number

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: true,
    })
    is_actived: boolean;

    @CreatedAt
    created_at: Date;

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    created_by: object;

    @UpdatedAt
    updated_at: Date;

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    updated_by: object;

    @AfterCreate
    @AfterUpdate
    static AfterChange = (model) => {
        if (model._options.isNewRecord) {
            Category.eventEmitter.emit(Category.Events.CATEGORY_CREATED, model);
        } else {
            Category.eventEmitter.emit(Category.Events.CATEGORY_UPDATED, model);
        }
    };

    @AfterDestroy
    static AfterDestroy = (model) => {
        Category.eventEmitter.emit(Category.Events.CATEGORY_DELETED, model);
    };

    /**
     * Event Bus
     */
    static sequelize: Sequelize = null;
    static eventEmitter: EventEmitter2 = null;
    static EVENT_SOURCE = `${environment.serviceName}.Category`;
    static Events = {
        CATEGORY_CREATED: `${environment.serviceName}.category.created`,
        CATEGORY_UPDATED: `${environment.serviceName}.category.updated`,
        CATEGORY_DELETED: `${environment.serviceName}.category.deleted`,
    };

    /**
     * Category Status
     */
    static Statuses = {
        1: {
            KEY: 1,
            NAME: 'Đang hoạt động',
        },
        2: {
            KEY: 2,
            NAME: 'Ngừng hoạt động',
        },
    };

    /**
     * Transform model to expose object
     */
    static transform(params) {
        const transformed = {} as any;
        const fields = [
            'id',
            'name',
            'status',
            'parent_id',
            'created_by',
            'updated_by',
        ];

        fields.forEach((field) => {
            transformed[field] = params[field];
        });

        transformed.status_name = Category.Statuses[transformed.status].NAME;
        transformed.created_at = moment(params.created_at).unix();
        transformed.updated_at = moment(params.updated_at).unix();

        return transformed;
    }

    /**
     * Create a category
     * @param {Category} payload
     */
    static async createCategory(payload) {
        try {
            const newCategory = await Category.create(payload);
            return newCategory;
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    /**
     * Update category by id
     * @param {*} CategoryObject
     */
    static async updateCategory(CategoryObject) {
        try {
            const changed = CategoryObject.changed();
            const newCategory = await CategoryObject.save({
                fields: changed,
                hooks: true,
            });
            return newCategory;
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    /**
     * List Categorys in descending order of 'created_at' timestamp.
     *
     * @param {number} skip - Number of Categorys to be skipped.
     * @param {number} limit - Limit number of Categorys to be returned.
     * @returns {Promise<Category[]>}
     */
    static list({
        limit = 20,
        offset = 0,
        sort_by = 'created_at',
        order_by = 'desc',
        statuses,
        category_name,
        min_created_at,
        max_created_at,
    }) {
        const options = filterConditions({
            statuses,
            category_name,
            min_created_at,
            max_created_at,
        });
        const sort = sortConditions({
            sortBy: sort_by,
            orderBy: order_by,
        });

        return Category.findAll({
            where: options,
            order: [sort],
            offset,
            limit,
        });
    }
    /**
     * Total Categorys.
     *
     * @param {*} params - Request params
     * @returns {Promise<Number>}
     */
    static getTotalItems({
        statuses,
        category_name,
        min_created_at,
        max_created_at,
    }) {
        const options = filterConditions({
            statuses,
            category_name,
            min_created_at,
            max_created_at,
        });
        return Category.count({
            where: options,
        });
    }

    /**
     * Get Category by id.
     * @param {*}
     * @returns {Promise<Category>}
     */
    static async getCategory(id) {
        const getCategory = await Category.findOne({
            where: {
                id,
                is_actived: true,
            },
        });
        if (getCategory) {
            return getCategory;
        }
        throw new NotFoundException({
            message: 'Danh mục không tồn tại',
            status: HttpStatus.NOT_FOUND,
        });
    }
}

// Filter by condition
function filterConditions(params) {
    const options = omitBy(params, isNil) as any;

    options.is_actived = true;

    if (options.category_name) {
        options.name = {
            [Op.iLike]: `%${options.category_name}%`
        }
    }
    delete options.category_name;

    if (options.statuses && options.statuses !== '0') {
        options.status = {
            [Op.in]: options.statuses.split(','),
        };
    }
    delete options.statuses;

    checkMinMaxOfConditionFields(
        options,
        'created_at',
        options.date_type || 'Date'
    );

    return options;
}

// Sort by condition
function sortConditions({ sortBy, orderBy }) {
    let sort = null;
    if (sortBy === 'updated_at') {
        sort = ['updated_at', orderBy];
    } else {
        sort = ['created_at', 'DESC'];
    }
    return sort;
}

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        const start = new Date(options[`min_${field}`]);
        _min = start.setHours(0, 0, 0, 0);

        const end = new Date(options[`max_${field}`]);
        _max = end.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) || !isNil(options[`max_${field}`])) {
        if (options[`min_${field}`] && !options[`max_${field}`]) {
            options[field] = {
                [Op.gte]: _min,
            };
        } else if (!options[`min_${field}`] && options[`max_${field}`]) {
            options[field] = {
                [Op.lte]: _max,
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0,
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}
