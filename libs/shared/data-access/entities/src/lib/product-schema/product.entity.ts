import * as moment from 'moment';
import { environment } from '@app/shared/environments';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Op, Sequelize } from 'sequelize';
import { isNil, omitBy, capitalize } from 'lodash';
import { HttpStatus, NotFoundException, HttpException } from '@nestjs/common';
import {
    AfterCreate,
    AfterUpdate,
    AfterDestroy,
    Column,
    CreatedAt,
    DataType,
    Model,
    Table,
    UpdatedAt,
} from 'sequelize-typescript';

@Table({ tableName: 'tbl_products' })
export class Product extends Model {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id: number;

    @Column({
        type: DataType.STRING(255),
        allowNull: false,
    })
    name: string;

    @Column({
        type: DataType.STRING(255),
        unique: true,
        allowNull: false,
    })
    sku: string;

    @Column({
        type: DataType.STRING(255),
    })
    slug: string;

    @Column({
        type: DataType.INTEGER,
        defaultValue: 1,
        allowNull: true,
    })
    status: number;

    @Column({
        type: DataType.INTEGER,
        defaultValue: 1,
        allowNull: true,
    })
    type: number;

    @Column({
        type: DataType.STRING(255),
        allowNull: true,
    })
    thumbnail: string;

    @Column({
        type: DataType.ARRAY(DataType.TEXT),
        defaultValue: [],
    })
    suppliers: string[]

    @Column({
        type: DataType.ARRAY(DataType.STRING(255)),
        defaultValue: [],
    })
    images: string[];

    @Column({
        type: DataType.STRING,
        defaultValue: null,
    })
    unit: string;

    @Column({
        type: DataType.DECIMAL,
        defaultValue: 0,
    })
    price: number;

    @Column({
        type: DataType.DECIMAL,
        defaultValue: 0,
    })
    original_price: number;

    @Column({
        type: DataType.DECIMAL,
        defaultValue: 0,
    })
    normal_price: number;

    @Column({
        type: DataType.ARRAY(DataType.TEXT),
    })
    categories: string[]

    @Column({
        type: DataType.JSONB,
        defaultValue: [],
    })
    variations: object[]

    @Column({
        type: DataType.ARRAY(DataType.STRING),
        defaultValue: [],
    })
    attributes: object[]

    @Column({
        type: DataType.STRING,
        defaultValue: null,
    })
    brand_id: number

    @Column({
        type: DataType.STRING,
        defaultValue: null,
    })
    brand_name: string

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    discount: object

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    flash_deal: object

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    hot_deal: object

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    campaign: object

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: true,
    })
    is_actived: boolean;

    @CreatedAt
    created_at: Date;

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    created_by: object;

    @UpdatedAt
    updated_at: Date;

    @Column({
        type: DataType.JSONB,
        defaultValue: null,
    })
    updated_by: object;

    @AfterCreate
    @AfterUpdate
    static AfterChange = (model) => {
        if (model._options.isNewRecord) {
            Product.eventEmitter.emit(Product.Events.PRODUCT_CREATED, model);
        } else {
            Product.eventEmitter.emit(Product.Events.PRODUCT_UPDATED, model);
        }
    };

    @AfterDestroy
    static AfterDestroy = (model) => {
        Product.eventEmitter.emit(Product.Events.PRODUCT_DELETED, model);
    };

    /**
     * Event Bus
     */
    static sequelize: Sequelize = null;
    static eventEmitter: EventEmitter2 = null;
    static EVENT_SOURCE = `${environment.serviceName}.product`;
    static Events = {
        PRODUCT_CREATED: `${environment.serviceName}.product.created`,
        PRODUCT_UPDATED: `${environment.serviceName}.product.updated`,
        PRODUCT_DELETED: `${environment.serviceName}.product.deleted`,
    };

    /**
     * Product Status
     */
    static Statuses = {
        1: {
            KEY: 1,
            NAME: 'Đang hoạt động',
        },
        2: {
            KEY: 2,
            NAME: 'Ngừng hoạt động',
        },
    };

    /**
     * Product Status
     */
    static Types = {
        1: {
            KEY: 1,
            NAME: 'Dịch vụ',
        },
        2: {
            KEY: 2,
            NAME: 'Khác',
        },
    };

    /**
     * Transform model to expose object
     */
    static transform(params) {
        const transformed = {} as any;
        const fields = [
            'id',
            'type',
            'status',
            'name',
            'sku',
            'thumbnail',
            'price',
            'original_price',
            'images',
            'created_by',
            'updated_by',
        ];

        fields.forEach((field) => {
            transformed[field] = params[field];
        });

        const decimalFields = ['original_price', 'price'];

        decimalFields.forEach((field) => {
            transformed[field] = parseInt(params[field]);
        });

        transformed.status_name = Product.Statuses[transformed.status].NAME;
        transformed.type_name = Product.Types[transformed.type].NAME;
        transformed.created_at = moment(params.created_at).unix();
        transformed.updated_at = moment(params.updated_at).unix();

        return transformed;
    }

    /**
     * Create Product
     * @param {Product} payload
     */
    static async createProduct(payload) {
        try {
            const newProduct = await Product.create(payload);
            return newProduct;
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    /**
     * Create many product
     * @param {Product} payload
     */
    static async bulkCreateProduct(payload) {
        try {
            const product = payload.map((i) => {
                return i.product;
            });
            const newProduct = await Product.bulkCreate(product);
            return newProduct;
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    /**
     * Update product by id
     * @param {*} productObject
     */
    static async updateProduct(productObject) {
        try {
            const changed = productObject.changed();
            const newProduct = await productObject.save({
                fields: changed,
                hooks: true,
            });
            return newProduct;
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    /**
     * List Products in descending order of 'created_at' timestamp.
     *
     * @param {number} skip - Number of Products to be skipped.
     * @param {number} limit - Limit number of Products to be returned.
     * @returns {Promise<Product[]>}
     */
    static list({
        limit = 20,
        offset = 0,
        sort_by = 'created_at',
        order_by = 'desc',
        note,
        product_code,
        statuses,
        suppliers,
        min_created_at,
        max_created_at,
    }) {
        const options = filterConditions({
            product_code,
            note,
            statuses,
            suppliers,
            min_created_at,
            max_created_at,
        });
        const sort = sortConditions({
            sortBy: sort_by,
            orderBy: order_by,
        });

        return Product.findAll({
            where: options,
            order: [sort],
            offset,
            limit,
        });
    }
    /**
     * Total Products.
     *
     * @param {*} params - Request params
     * @returns {Promise<Number>}
     */
    static getTotalItems({
        note,
        product_code,
        statuses,
        suppliers,
        min_created_at,
        max_created_at,
    }) {
        const options = filterConditions({
            product_code,
            note,
            statuses,
            suppliers,
            min_created_at,
            max_created_at,
        });
        return Product.count({
            where: options,
        });
    }

    /**
     * Get Product by id.
     * @param {*}
     * @returns {Promise<Product>}
     */
    static async getProduct(id) {
        const product = await Product.findOne({
            where: {
                id,
                is_actived: true,
            },
        });
        if (product) {
            return product;
        }
        throw new NotFoundException({
            message: 'Sản phẩm không tồn tại',
            status: HttpStatus.NOT_FOUND,
        });
    }
}

// Filter by condition
function filterConditions(params) {
    const options = omitBy(params, isNil) as any;

    options.is_actived = true;

    if (options.product_code) {
        options[Op.or] = {
            name: { [Op.iLike]: `%${options.product_code}%` },
            sku: { [Op.iLike]: `%${options.product_code}%` },
        };
    }
    delete options.product_code;

    if (options.statuses && options.statuses !== '0') {
        options.status = {
            [Op.in]: options.statuses.split(','),
        };
    }
    delete options.statuses;

    if (options.suppliers && options.suppliers !== '0') {
        options.supplier_id = {
            [Op.in]: options.suppliers.split(','),
        };
    }
    delete options.suppliers;

    if (options.types && options.types !== '0') {
        options.type = {
            [Op.in]: options.types,
        };
    }
    delete options.types;

    checkMinMaxOfConditionFields(
        options,
        'created_at',
        options.date_type || 'Date'
    );

    return options;
}

// Sort by condition
function sortConditions({ sortBy, orderBy }) {
    let sort = null;
    if (sortBy === 'updated_at') {
        sort = ['updated_at', orderBy];
    } else {
        sort = ['created_at', 'DESC'];
    }
    return sort;
}

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        const start = new Date(options[`min_${field}`]);
        _min = start.setHours(0, 0, 0, 0);

        const end = new Date(options[`max_${field}`]);
        _max = end.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) || !isNil(options[`max_${field}`])) {
        if (options[`min_${field}`] && !options[`max_${field}`]) {
            options[field] = {
                [Op.gte]: _min,
            };
        } else if (!options[`min_${field}`] && options[`max_${field}`]) {
            options[field] = {
                [Op.lte]: _max,
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0,
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}
