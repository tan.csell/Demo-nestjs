/* eslint-disable */
import '@nestjs/common';
require('dotenv').config({ path: '.env' });

export const environment = {
    port: process.env.PORT,
    nodeEnv: process.env.NODE_ENV,
    postgresUri:
        process.env.NODE_ENV === 'development'
            ? process.env.POSTGRESQL_URI_TEST
            : process.env.POSTGRESQL_URI,
    redisUri:
        process.env.NODE_ENV === 'development'
            ? process.env.REDIS_URI_TEST
            : process.env.REDIS_URI,
    fileSizeLimit: process.env.FILE_SIZE_LIMIT
        ? parseInt(process.env.FILE_SIZE_LIMIT, 10)
        : 1024 * 1024 * 5,
    serviceName: 'demo_service',
};
