import { Category } from '@app/shared/data-access/entities';
import { BadRequestException } from '@nestjs/common';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class CategoryMiddleware {
    readonly model = this.category;

    constructor(
        @InjectModel(Category)
        private category: typeof Category
    ) { }
}

/**
 * Load Category append to locals
 */
export class LoadCategory extends CategoryMiddleware implements NestMiddleware {
    async use(req, res, next) {
        const Category = await this.model.getCategory(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.Category = Category;
        next();
    }
}

/**
 * Load total Category
 */
export class CountCategory extends CategoryMiddleware implements NestMiddleware {
    async use(req, res, next) {
        const count = await this.model.getTotalItems(req.query);
        req.locals = req.locals ? req.locals : {};
        req.locals.count = count;
        next();
    }
}
