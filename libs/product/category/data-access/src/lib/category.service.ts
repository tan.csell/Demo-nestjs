import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Category } from '@app/shared/data-access/entities';
import {
    CategoryCreateParamsDto,
    CategoryUpdateParamsDto,
} from '@app/shared/data-access/models';

@Injectable()
export class CategoryService {
    constructor(
        private eventEmitter: EventEmitter2,
        @InjectModel(Category) private model: typeof Category
    ) {
        this.model.eventEmitter = this.eventEmitter;
    }

    /**
     * Create single Category
     * @param {*} Category
     * @returns
     */
    saveCategory(Category: CategoryCreateParamsDto) {
        return this.model.createCategory(Category);
    }

    /**
     * Update single Category
     * @param {*} Category
     * @returns
     */
    updateCategory(Category: CategoryUpdateParamsDto) {
        return this.model.updateCategory(Category);
    }

    /**
     * Create multiple Category
     * @param {*} Categories
     * @returns
     */
    bulkCreateCategories(Categories: CategoryCreateParamsDto[]) {
        return this.model.bulkCreate(Categories as any[]);
    }

    /**
     * Get all Category from query
     * @param {*} query
     * @returns
     */
    getAllCategory(query) {
        return this.model.list(query);
    }

    /**
     * Total Category from query
     * @param {*} query
     * @returns
     */
    getTotalCategory(query) {
        return this.model.getTotalItems(query);
    }

    /**
     * Get Category from query
     * @param {*} query
     * @returns
     */
    getCategory(query) {
        return this.model.getCategory(query);
    }
}
