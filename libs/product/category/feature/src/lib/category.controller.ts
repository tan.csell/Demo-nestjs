import { HttpException, HttpStatus } from '@nestjs/common';
import { pick } from 'lodash';
import {
    Body,
    Controller,
    Get,
    Post,
    Delete,
    Patch,
    Query,
    Request,
    UsePipes,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiOkResponse,
    ApiParam,
    ApiResponse,
    ApiSecurity,
    ApiTags,
} from '@nestjs/swagger';

import { AuthValidationPipe, UseRoles } from '@app/shared/auth-config';
import {
    // CategoryCreateValidator,
    // CategoryListValidator,
    CategoryService,
    // CategoryUpdateValidator
} from '@app/product/category/data-access';
import {
    CategoryCreateParamsDto,
    CategoryDto,
    CategoryListParamsDto,
    CategoryUpdateParamsDto,
} from '@app/shared/data-access/models';
import { Category } from '@app/shared/data-access/entities';

@ApiTags('Categorys')
@ApiBearerAuth()
@ApiSecurity('X-XSRF-TOKEN')
@Controller('/v1/Categorys')
export class CategoryController {
    constructor(private service: CategoryService) { }

    @Get()
    // @UsePipes(new AuthValidationPipe(CategoryListValidator))
    @ApiOkResponse({ type: CategoryDto })
    async list(@Query() query: CategoryListParamsDto, @Request() req) {
        const Categorys = await this.service.getAllCategory(query);
        return {
            code: 0,
            count: req.locals.count,
            data: Categorys.map(Category.transform),
        };
    }

    @Post()
    // @UsePipes(new AuthValidationPipe(CategoryCreateValidator))
    @ApiOkResponse({ type: CategoryDto })
    async create(@Body() body: CategoryCreateParamsDto, @Request() req) {
        try {
            const data = Object.assign(body, {
                created_by: pick(req.user, ['id', 'name']),
            });
            const newCategory = await this.service.saveCategory(data);
            return {
                code: 0,
                data: Category.transform(newCategory),
                message: 'Thêm mới danh mục sản phẩm thành công',
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Patch(':id')
    // @UsePipes(new AuthValidationPipe(CategoryUpdateValidator))
    @ApiResponse({ type: CategoryDto })
    @ApiParam({ name: 'id', type: String, example: 1 })
    async update(@Request() req, @Body() body: CategoryUpdateParamsDto) {
        try {
            const Category = Object.assign(req.locals.Category, body, {
                updated_by: pick(req.user, ['id', 'name']),
            });
            const newCategory = await this.service.updateCategory(Category);
            return {
                code: 0,
                data: Category.transform(newCategory),
                message: 'Cập nhật danh mục sản phẩm thành công',
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Get(':id')
    @ApiOkResponse({ type: CategoryDto })
    @ApiParam({ name: 'id', type: String, example: 1 })
    async get(@Request() req) {
        return {
            code: 0,
            data: Category.transform(req.locals.Category),
        };
    }

    @Delete(':id')
    @ApiParam({ name: 'id', type: String, example: 1 })
    async delete(@Request() req) {
        try {
            const obj = Object.assign(req.locals.Category, {
                is_actived: false,
                updated_by: pick(req.user, ['id', 'name']),
            });
            const newCategory = await this.service.updateCategory(obj);
            return {
                code: 0,
                message: 'Xoá danh mục sản phẩm thành công',
                data: Category.transform(newCategory),
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
