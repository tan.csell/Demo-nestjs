import { Module, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { CategoryController } from "./category.controller";
import {
    CategoryMiddleware,
    CategoryService,
    CountCategory,
    LoadCategory,
} from "@app/product/category/data-access";

@Module({
    controllers: [CategoryController],
    providers: [CategoryService, CategoryMiddleware],
    exports: [],
})
export class ApiCategoryFeatureModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(CountCategory).forRoutes({
            path: '/v1/categories',
            method: RequestMethod.GET,
        });

        consumer.apply().forRoutes({
            path: '/v1/categories',
            method: RequestMethod.POST,
        });

        consumer.apply(LoadCategory).forRoutes({
            path: '/v1/categories/:id',
            method: RequestMethod.GET,
        });

        consumer.apply(LoadCategory).forRoutes({
            path: '/v1/categories/:id',
            method: RequestMethod.PATCH,
        });

        consumer.apply(LoadCategory).forRoutes({
            path: '/v1/categories/:id',
            method: RequestMethod.DELETE,
        });
    }
}
