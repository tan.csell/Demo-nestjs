import { HttpException, HttpStatus } from '@nestjs/common';
import { pick } from 'lodash';
import {
    Body,
    Controller,
    Get,
    Post,
    Delete,
    Patch,
    Query,
    Request,
    UsePipes,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiOkResponse,
    ApiParam,
    ApiResponse,
    ApiSecurity,
    ApiTags,
} from '@nestjs/swagger';

import { AuthValidationPipe, UseRoles } from '@app/shared/auth-config';
import {
    ProductCreateValidator,
    ProductListValidator,
    ProductService,
    ProductUpdateValidator
} from '@app/product/product/data-access';
import {
    ProductCreateParamsDto,
    ProductDto,
    ProductListParamsDto,
    ProductUpdateParamsDto,
} from '@app/shared/data-access/models';
import { Product } from '@app/shared/data-access/entities';

@ApiTags('Products')
@ApiBearerAuth()
@ApiSecurity('X-XSRF-TOKEN')
@Controller('/v1/products')
export class ProductController {
    constructor(private service: ProductService) { }

    @Get()
    @UsePipes(new AuthValidationPipe(ProductListValidator))
    @ApiOkResponse({ type: ProductDto })
    async list(@Query() query: ProductListParamsDto, @Request() req) {
        const products = await this.service.getAllProduct(query);
        return {
            code: 0,
            count: req.locals.count,
            data: products.map(Product.transform),
        };
    }

    @Post()
    @UsePipes(new AuthValidationPipe(ProductCreateValidator))
    @ApiOkResponse({ type: ProductDto })
    async create(@Body() body: ProductCreateParamsDto, @Request() req) {
        try {
            const product = Object.assign(body, {
                created_by: pick(req.user, ['id', 'name']),
            });
            const newProduct = await this.service.saveProduct(product);
            return {
                code: 0,
                data: Product.transform(newProduct),
                message: 'Thêm mới sản phẩm thành công',
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Patch(':id')
    @UsePipes(new AuthValidationPipe(ProductUpdateValidator))
    @ApiResponse({ type: ProductDto })
    @ApiParam({ name: 'id', type: String, example: 1 })
    async update(@Request() req, @Body() body: ProductUpdateParamsDto) {
        try {
            const product = Object.assign(req.locals.product, body, {
                updated_by: pick(req.user, ['id', 'name']),
            });
            const newProduct = await this.service.updateProduct(product);
            return {
                code: 0,
                data: Product.transform(newProduct),
                message: 'Cập nhật sản phẩm thành công',
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Get(':id')
    @ApiOkResponse({ type: ProductDto })
    @ApiParam({ name: 'id', type: String, example: 1 })
    async get(@Request() req) {
        return {
            code: 0,
            data: Product.transform(req.locals.product),
        };
    }

    @Delete(':id')
    @ApiParam({ name: 'id', type: String, example: 1 })
    async delete(@Request() req) {
        try {
            const obj = Object.assign(req.locals.product, {
                is_actived: false,
                updated_by: pick(req.user, ['id', 'name']),
            });
            const newProduct = await this.service.updateProduct(obj);
            return {
                code: 0,
                message: 'Xoá sản phẩm thành công',
                data: Product.transform(newProduct),
            };
        } catch (error) {
            throw new HttpException({ error }, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
