import {
    CountProduct,
    LoadProduct,
    PrepareProductCreate,
    ProductMiddleware,
    ProductService,
} from '@app/product/product/data-access';
import { Module, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { ProductController } from './product.controller';

@Module({
    controllers: [ProductController],
    providers: [ProductService, ProductMiddleware],
    exports: [],
})
export class ApiProductFeatureModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(CountProduct).forRoutes({
            path: '/v1/products',
            method: RequestMethod.GET,
        });

        consumer.apply(PrepareProductCreate).forRoutes({
            path: '/v1/products',
            method: RequestMethod.POST,
        });

        consumer.apply(LoadProduct).forRoutes({
            path: '/v1/products/:id',
            method: RequestMethod.GET,
        });

        consumer.apply(LoadProduct).forRoutes({
            path: '/v1/products/:id',
            method: RequestMethod.PATCH,
        });

        consumer.apply(LoadProduct).forRoutes({
            path: '/v1/products/:id',
            method: RequestMethod.DELETE,
        });
    }
}
