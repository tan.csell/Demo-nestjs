import * as Joi from 'joi';

export const ProductListValidator = Joi.object()
    .keys({
        offset: Joi.number().default(0).allow(null, ''),
        limit: Joi.number().default(20).allow(null, ''),
        sort_by: Joi.string().allow(null, ''),
        order_by: Joi.string().allow(null, ''),
        product_keyword: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo tên và mã sản phẩm'
                )
            ),
        types: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo loại sản phẩm'
                )
            ),
        statuses: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo trạng thái sản phẩm'
                )
            ),
        suppliers: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo nhà cung cấp sản phẩm'
                )
            ),
        attributes: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo thuộc tính sản phẩm'
                )
            ),
        categories: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo danh mục sản phẩm'
                )
            ),
        brand: Joi.string()
            .trim()
            .allow(null, '')
            .error(
                new Error(
                    'Vui lòng nhập đúng định dạng bộ lọc theo thương hiệu sản phẩm'
                )
            ),
    })
    .unknown(true);

export const ProductCreateValidator = Joi.object().keys({
    note: Joi.string()
        .trim()
        .max(255)
        .allow(null, '')
        .error(new Error('Vui lòng nhập ghi chú sản phẩm tối đa 255 ký tự')),
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(new Error('Vui lòng nhập tên sản phẩm và tối đa 255 ký tự')),
    sku: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(new Error('Vui lòng nhập mã sản phẩm và tối đa 255 ký tự')),
    status: Joi.number()
        .error(new Error('Vui lòng nhập đúng định dạng trạng thái sản phẩm')),
    type: Joi.number()
        .allow(null, '')
        .error(new Error('Vui lòng chọn loại sản phẩm')),
    suppliers: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin nhà cung cấp')),
    attributes: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin thuộc tính sản phẩm')),
    variations: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin phân loại sản phẩm')),
    price: Joi.number()
        .default(0)
        .error(new Error('Vui lòng nhập đúng định dạng giá bán sản phẩm')),
    normal_price: Joi.number()
        .default(0)
        .error(new Error('Vui lòng nhập đúng định dạng giá niêm yết sản phẩm')),
    original_price: Joi.number()
        .default(0)
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng giá gốc sản phẩm')),
    thumbnail: Joi.string()
        .trim()
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng ảnh sản phẩm')),
    description: Joi.string()
        .trim()
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng mô tả sản phẩm')),
    is_visible: Joi.boolean()
        .error(new Error('Vui lòng nhập đúng định dạng trạng thái hiển thị trên web')),
    images: Joi.array()
        .allow(null, '[]')
        .error(
            new Error('Vui lòng nhập đúng định dạng danh sách ảnh sản phẩm')
        ),
});

export const ProductUpdateValidator = Joi.object().keys({
    note: Joi.string()
        .trim()
        .max(255)
        .allow(null, '')
        .error(new Error('Vui lòng nhập ghi chú sản phẩm tối đa 255 ký tự')),
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(new Error('Vui lòng nhập tên sản phẩm và tối đa 255 ký tự')),
    sku: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(new Error('Vui lòng nhập mã sản phẩm và tối đa 255 ký tự')),
    status: Joi.number()
        .error(new Error('Vui lòng nhập đúng định dạng trạng thái sản phẩm')),
    type: Joi.number()
        .allow(null, '')
        .error(new Error('Vui lòng chọn loại sản phẩm')),
    suppliers: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin nhà cung cấp')),
    attributes: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin thuộc tính sản phẩm')),
    variations: Joi.array()
        .allow(null, '[]')
        .error(new Error('Vui lòng nhập đúng thông tin phân loại sản phẩm')),
    price: Joi.number()
        .default(0)
        .error(new Error('Vui lòng nhập đúng định dạng giá bán sản phẩm')),
    normal_price: Joi.number()
        .default(0)
        .error(new Error('Vui lòng nhập đúng định dạng giá niêm yết sản phẩm')),
    original_price: Joi.number()
        .default(0)
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng giá gốc sản phẩm')),
    thumbnail: Joi.string()
        .trim()
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng ảnh sản phẩm')),
    description: Joi.string()
        .trim()
        .allow(null, '')
        .error(new Error('Vui lòng nhập đúng định dạng mô tả sản phẩm')),
    is_visible: Joi.boolean()
        .error(new Error('Vui lòng nhập đúng định dạng trạng thái hiển thị trên web')),
    images: Joi.array()
        .allow(null, '[]')
        .error(
            new Error('Vui lòng nhập đúng định dạng danh sách ảnh sản phẩm')
        ),
});
