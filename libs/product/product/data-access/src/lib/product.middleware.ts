import { Product } from '@app/shared/data-access/entities';
import { BadRequestException } from '@nestjs/common';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ProductService } from './product.service';

@Injectable()
export class ProductMiddleware {
    readonly productModel = this.product;
    readonly productService = this.service;

    constructor(
        private service: ProductService,
        @InjectModel(Product)
        private product: typeof Product
    ) { }
}

/**
 * Load Product append to locals
 */
export class LoadProduct extends ProductMiddleware implements NestMiddleware {
    async use(req, res, next) {
        const product = await this.productModel.getProduct(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.product = product;
        next();
    }
}

/**
 * Load total Product
 */
export class CountProduct extends ProductMiddleware implements NestMiddleware {
    async use(req, res, next) {
        const count = await this.productModel.getTotalItems(req.query);
        req.locals = req.locals ? req.locals : {};
        req.locals.count = count;
        next();
    }
}

/**
 * Perpare data befor create
 */
export class PrepareProductCreate extends ProductMiddleware implements NestMiddleware {
    async use(req, res, next) {
        if (req.body.price) {
            req.body.price = Number(req.body.price);
        } else {
            req.body.price = 0;
        }

        if (req.body.original_price) {
            req.body.original_price = Number(req.body.original_price);
        } else {
            req.body.original_price = 0;
        }

        next();
    }
}
