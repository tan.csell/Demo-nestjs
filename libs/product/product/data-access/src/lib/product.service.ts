import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Product } from '@app/shared/data-access/entities';
import {
    ProductCreateParamsDto,
    ProductUpdateParamsDto,
} from '@app/shared/data-access/models';

@Injectable()
export class ProductService {
    constructor(
        private eventEmitter: EventEmitter2,
        @InjectModel(Product) private productModel: typeof Product
    ) {
        this.productModel.eventEmitter = this.eventEmitter;
    }

    /**
     * Create single Product
     * @param {*} Product
     * @returns
     */
    saveProduct(product: ProductCreateParamsDto) {
        return this.productModel.createProduct(product);
    }

    /**
     * Update single Product
     * @param {*} Product
     * @returns
     */
    updateProduct(product: ProductUpdateParamsDto) {
        return this.productModel.updateProduct(product);
    }

    /**
     * Create multiple Product
     * @param {*} Products
     * @returns
     */
    bulkCreateProducts(products: ProductCreateParamsDto[]) {
        return this.productModel.bulkCreateProduct(products as any[]);
    }

    /**
     * Get all Product from query
     * @param {*} query
     * @returns
     */
    getAllProduct(query) {
        return this.productModel.list(query);
    }

    /**
     * Total Product from query
     * @param {*} query
     * @returns
     */
    getTotalProduct(query) {
        return this.productModel.getTotalItems(query);
    }

    /**
     * Get Product from query
     * @param {*} query
     * @returns
     */
    getProduct(query) {
        return this.productModel.getProduct(query);
    }
}
