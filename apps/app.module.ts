import { SharedEntitiesModule } from '@app/shared/data-access/entities';
import { PersistentConfigModule } from '@app/shared/persistent-config';
import { Module } from '@nestjs/common';
// App components
import { AppController } from './app/app.controller';
import { AppService } from './app/app.service';
import { ApiProductFeatureModule } from '@app/product/product/feature';
import { ApiCategoryFeatureModule } from '@app/product/category/feature';

@Module({
    imports: [
        PersistentConfigModule,
        SharedEntitiesModule,
        ApiProductFeatureModule,
        ApiCategoryFeatureModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule { }
